import loads from './components/loads.vue';
import orders from './components/orders.vue';
import home from './components/home.vue';
import header from './components/header.vue';

export default [
    { path: '/', component: home },
    { path: '/loads', component: loads },
    { path: '/orders', component: orders },
    { path: '/header', component: header }
]