import { http } from './config';

export default {
  index() {
    return http.get('orders');
  },
  show() {
    return http.get('orders/id');
  }
};
