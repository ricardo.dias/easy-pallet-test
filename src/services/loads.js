import { http } from './config';

export default {
  index() {
    return http.get('loads');
  },
  show() {
    return http.get('loads/id');
  }
};
