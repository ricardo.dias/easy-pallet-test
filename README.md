# Test Project for Easy Pallet (Front-End)

**Easy Pallet Test Project developed by Ricardo Lima**

O front-end para listagem dos dados foi desenvolvido utilizando Vue.JS

![Easy Pallet](./src/assets/logo.png "Easy Pallet")

## Dependecies

Must have installed:

- Yarn version 1.17.3
- Nodejs version v8.10.0

## Project setup

Clone the project:

```
git clone git@gitlab.com:ricardo.dias/easy-pallet-test.git
```

Enter the project folder:

```
cd easy-pallet-test/
```

Run:

```
yarn install
```

### Run the server under development

```
yarn run serve
```

### Access URL

```
http://localhost:8080
```

# Note
Remember that the API must be up.
